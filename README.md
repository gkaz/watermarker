# Watermarker

A simple tool intended to add watermarks to FreeCAD screenshots.

It can paste watermarks generated from text or image into the screenshot of FreeCAD's work area. You're able to select watermark opacity, exact X/Y placement, and some other useful options.

### Installation

Full installation instructions on [official wiki](https://wiki.freecadweb.org/How_to_install_macros).

You should copy either `watermarker-full.FCMacro` or `watermarker-lite.FCMacro` into FreeCAD macros folder.

Macros folder location is:

- `/home/username/.FreeCAD/Macro` on Linux
- `C:\Users\username\AppData\Roaming\FreeCAD\` on Windows
- `/Users/username/Library/Preferences/FreeCAD/` on MacOS
- Some other directory, if you've specified one in `Macro -> Macros...` menu window before.

### Overview

![](assets/watermarker-overview-1.png)